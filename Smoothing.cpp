#include <iostream>
#include "opencv2/core/core.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"
#include <sstream>
#include <string>
#include <fstream>
#include <vector>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <map>

#define PI 3.1415926535897932384626433832795

using namespace std;
using namespace cv;

/// Global Variables
char window_name[] = "Filter Demo 1";
//ofstream salida;

/// Function headers
double distancia(Point C, Point a);
double arccos(Point c, Point a, int idx);
void printGrupos(std::vector<std::vector<cv::Point> > grupos);
double range(std::vector<std::vector<cv::Point> > grupos, int grupo, cv::Point centro);
double dMin(std::vector<std::vector<cv::Point> > grupos, int grupo, cv::Point centro);
double dMax(std::vector<std::vector<cv::Point> > grupos, int grupo, cv::Point centro);
std::vector<double> extraeVector( std::string pathImagen);
/**
 * function main
 */
 int main( int argc, char** argv )
 {	
	std::string nombreArchivo;
	std::string linea, rangos, nombre, accion_camara;
	std::string datos = "/media/psf/Home/Documents/MAGISTER/ProyectoTesis/Data/DatosEntrenamiento/dataSetCompleto/datos";
	std::string path = "/media/psf/Home/Documents/MAGISTER/ProyectoTesis/Data/DatosEntrenamiento/dataSetCompleto/";
	std::string data = "/media/psf/Home/Documents/MAGISTER/ProyectoTesis/Data/";
	char directorio[1024], archivos[1024];
	size_t found;
	
	
	strcpy(directorio, datos.c_str());
	std::ifstream infile(directorio);
	 
	while (std::getline(infile, linea)){
		 cout << linea << endl;
		 found = linea.find("_");
		 nombre = linea.substr(0,found); // directorio donde estara la imagen buscada
		 accion_camara = linea.substr(found+1, linea.length());
		 cout <<  "accion camara" << accion_camara << endl;
		 strcpy(directorio, datos.c_str());
	     strcpy(archivos, (path+linea).c_str());
	     std::ifstream inArchivo(archivos);
	     if(inArchivo.fail()){
			 cout << "falla al leer el archivo" << endl;
		 }
	     //cout << archivos << endl
	     while (std::getline(inArchivo, rangos)){
			int persona, a, b;
			std::stringstream ss;
			ss.str(rangos);
			std::string item;
			std::getline(ss, item, ' ');
			persona = atoi(item.c_str());
			ofstream fs((accion_camara+'_'+item).c_str());
			std::getline(ss, item, ' ');
			a = atoi(item.c_str());
			std::getline(ss, item, ' ');
			b = atoi(item.c_str());
			
			 
			// Enviamos una cadena al fichero de salida:
			
			//cout << "persona: " << persona << " a: "<< a << " b: " << b <<   endl;
		    // leer los archivos para sacar los rangos
		    //  cout << "rango: "<<rangos << endl;
		    // cout << "rango: "<<rangos.substr(0, persona) << "-" << rangos.substr(persona+1, a)  <<"-" << rangos.substr(a+3, rangos.size()) << endl;
		    // abro archivo para dejar las caracteristicas por separado
		    for(int i=a; i <= b; i++){
				// ir guardando v en un archivo
				stringstream convert; // stringstream used for the conversion
				convert << i;//add the value of Number to the characters in the stream
				std:string imagen = data + linea.substr(0,found) +"/"+ convert.str()+".png";
				std::vector<double> v = extraeVector(imagen);
				//cout << "persona: " << persona << endl;
				for(int j=1; j < 19 ; j++){
					//printf(" %lf",v[j]);
					fs << v[j] << " " ;
					// Cerrar el fichero, 
					// para luego poder abrirlo para lectura:
				}	
				fs << endl;
				//cout << endl;
			}
			fs.close(); 
	     }
	     
	     inArchivo.close();
	  }
    waitKey(0);
    return 0;
 }
 
 
std::vector<double> extraeVector( std::string pathImagen){
	Mat image = imread( pathImagen, 1 );
	
	
	
	std::vector<std::vector<cv::Point> > contours, clone, grupos(19);
	std::vector<cv::Point> temp;
	std::vector<double> v(19), V(19);
	if(! image.data){
		 cout << "error al leer la imagen" << pathImagen << endl;
		 return V;
	}
	
	// procesamiento básico de estala de grices y binarización (esto es solo por los tipos de datos porque no le hace nada a la imagen)
	cv::cvtColor(image, image, CV_BGR2GRAY);
    cv::threshold(image, image, 128, 255, CV_THRESH_BINARY);
    cv::medianBlur( image, image, 13 );
    cv::Mat contourOutput = image.clone();
    
    cv::findContours( contourOutput, contours, CV_RETR_LIST, CV_CHAIN_APPROX_NONE );
	clone = contours;
	
	//Draw the contours
    cv::Mat contourImage(image.size(), CV_8UC3, cv::Scalar(0,0,0));
    cv::Scalar colors[3];
    colors[0] = cv::Scalar(255, 0, 0);
    colors[1] = cv::Scalar(0, 255, 0);
    colors[2] = cv::Scalar(0, 0, 255);
    int indice = 0;
    double x,y;
    
    if(contours.size() == 0) return V ;
    // busco el contorno con mayor cantidad de puntos, este debe ser el contorno de la persona
    for (size_t idx = 1; idx < contours.size(); idx++) {
        if(contours[idx].size() > contours[indice].size()) indice = idx;
    }
    
    // sumo todos los x e y del contorno con mayor cantidad de puntos para sacar el centro de energía de la figura
    
    if(contours[indice].size() < 40) return V ;
    for (size_t idx = 0; idx < contours[indice].size(); idx++) {
        x = x + contours[indice][idx].x;
        y = y + contours[indice][idx].y;
    }
    // obtengo el punto centro de la figura
    Point centroide =  Point(x/contours[indice].size(),y/contours[indice].size());
    Point centroideCoor =  Point(0,0);
	  
    // obtengo el punto mas lejano del centro para usarlo como radio de la circunferencia
    double radio = 1.0;
    //salida.open("puntos.txt");
    
    for (size_t idx = 0; idx < contours[indice].size(); idx++) {
		contours[indice][idx].x = contours[indice][idx].x - centroide.x; // aplico el eje de coordinadas desde el centroide tomandolo como un centro (0,0)
        contours[indice][idx].y = contours[indice][idx].y - centroide.y;
        double dist = distancia(centroideCoor,contours[indice][idx]);
        int indiceGrupo = ceil((18*arccos(centroideCoor,contours[indice][idx], idx))/360); 
        if(indiceGrupo < 1 || indiceGrupo > 18) return V ;
        //cout << "indiceGrupo: " << indiceGrupo << " con: " << contours[indice].size() << endl;
        grupos[indiceGrupo].push_back(contours[indice][idx]);
        //salida << idx << " arccos(" << centroideCoor << "," << contours[indice][idx] << "): " << indiceGrupo << endl;
        if(dist > radio) radio = dist;
    }
    //salida << " radio: " << radio << endl;
    //salida.close();
    
    //printGrupos(grupos);  imprimiendo los puntos de los grupos
     double sumaV = 0.0;
     for(int i=1; i < 19; i++){
		 v[i] = range(grupos,i,centroideCoor)/i;
		 sumaV+= v[i];
	 }
     for(int i=1; i < 19; i++){
		 V[i] = v[i]/sumaV;
		 //printf(" %lf",v[i]);
					
	 }
	 //cout << endl;
	 
    // dibujando las lineas de división
   /* for (int i=0; i<360; i+=20)
	{
		double s = sin(i*CV_PI/180);
		double c = cos(i*CV_PI/180);
		Point p2(centroide.x+s*150, centroide.y+c*150); 
		line( contourImage, centroide, p2, Scalar(0,255,255), 1, CV_AA);
	}
    
    // dibujando las imagenes en las ventanas
    cv::drawContours(contourImage, clone, indice, colors[indice % 3]);
    cv::circle( contourImage, centroide, 2.0, Scalar( 0, 0, 255 ),CV_FILLED, 0, 8 );
    cv::circle( contourImage, centroide, radio, Scalar( 255, 0, 127 ),0, 5 );
    if ( !image.data )
    {
        printf("No image data \n");
        
    }
    cv::imshow("Imagen Original", image);
    cvMoveWindow("Imagen Original", 0, 0);
    cv::imshow("Contornos y gráficos", contourImage);
    cvMoveWindow("Contornos y gráficos", 720, 0);
    */
    return V;
 }
 

 
 void printGrupos(std::vector<std::vector<cv::Point> > grupos){
	 for(int i=1; i < 19; i++){
		 std::cout << "grupo: "<< i <<  '\n';
		 for (std::vector<cv::Point>::iterator it = grupos[i].begin(); it != grupos[i].end(); ++it)
			std::cout << ' ' << *it;
		  std::cout << '\n';
	 }
 }
 
 double distancia(Point centro, Point a){
	 return  sqrt (pow(centro.x-a.x, 2) + pow(centro.y-a.y, 2)); ;
 }
 
 double range(std::vector<std::vector<cv::Point> > grupos, int grupo, cv::Point centro){
	 return  dMax(grupos, grupo, centro) - dMin(grupos, grupo, centro);
 }
 
 double dMax(std::vector<std::vector<cv::Point> > grupos, int grupo, cv::Point centro){
	 double dist = 0.0, tmp=0.0;
	 for (size_t idx = 0; idx < grupos[grupo].size(); idx++) {
		tmp = distancia(centro,grupos[grupo][idx]);
        if(dist < tmp) dist = tmp;
    }
    return dist;
 }
 
  double dMin(std::vector<std::vector<cv::Point> > grupos, int grupo, cv::Point centro){
	 double dist = 0.0, tmp=0.0;
	 for (size_t idx = 0; idx < grupos[grupo].size(); idx++) {
		tmp = distancia(centro,grupos[grupo][idx]);
        if(dist > tmp) dist = tmp;
    }
    return dist;
 }
 
 double arccos(Point centro, Point a, int idx){
	 double number = (a.y - centro.y)/distancia(centro,a);
	 double valor = 0.0;
	 if(a.x > 0){
		valor = (acos(number))*(180/PI);
	 }
	 else{
		valor = 180 + (acos(number))*(180/PI);
	 }
	 return  valor;
 }
